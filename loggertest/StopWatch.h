#pragma once

class StopWatch {
public:
	StopWatch() {
		Reset();
	}
	void Reset() {
		QueryPerformanceCounter((LARGE_INTEGER *)&counter);
	}
	double Elapsed() const {
		UINT64 now, freq;
		QueryPerformanceCounter((LARGE_INTEGER *)&now);
		QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
		return (double)(now - counter) / (double)freq;
	}
private:
	UINT64 counter;
};