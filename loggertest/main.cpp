﻿#include <string>
#include "log.h"

#define Log_Bar(txt, ...) Log_Write("Bar", txt, __VA_ARGS__)

using Log::$;

UINT const my_exception_code = 0xFF123456;
auto msg = $("Raising exception 0X%08X", my_exception_code);

void test() {

	Log_Bar("Test");
	if (IsDebuggerPresent()) {
		Log_Bar("Debugger is present");
		__try {
			ULONG_PTR args[] = { 0xDEADBEEF };
			Log_Bar(msg);
			RaiseException(my_exception_code, 0, _countof(args), args);
			Log_Bar("Exception raised...");
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {
			Log_Bar("Exception handler...");
		}
	}
}

int main(int, TCHAR **) {

	char const *TAG = "main";
	
	Log_Debug("Some debug info");

	test();

	Log_Error($("An error has occurred: %08x", GetLastError()));

	Log_Warning("Hello, it's a warning!");

	for (int i = 0; i < 4; ++i) {
		Log_Write($("COM%d", i), $("Some com traffic on port %02x", i));
	}

	Log_Info("Finished");

	Log_Debug(TAG, "Hello");

	void test_threading();
	test_threading();

	return 0;
}
