#include <Windows.h>
#include <tchar.h>
#include <string>
#include <vector>
#include "log.h"
#include "StopWatch.h"

using Log::$;

#define Log_Foo(txt, ...) Log_Write("Foo", txt, __VA_ARGS__)

#define THREADCOUNT 64

double times[THREADCOUNT];
HANDLE aThread[THREADCOUNT];

DWORD WINAPI ThreadFunc(LPVOID time) {
	UINT_PTR id = reinterpret_cast<UINT_PTR>(time);
	StopWatch timer;
	Log_Write($(L"Thread%d", id), "Active");
	double time_taken = timer.Elapsed();
	Log_Debug("%d took %fms", id, time_taken * 1000.0);
	times[id] = time_taken;
	return 0;
}

void test_threading() {
	Log_Warning("Hello world from the warning channel! But then the message got longer");
	for (int i = 0; i < THREADCOUNT; ++i) {
		Log_Info("Creating Thread %d", i);
		DWORD ThreadID;
		aThread[i] = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadFunc, reinterpret_cast<LPVOID>((UINT_PTR)i), 0, &ThreadID);
	}
	Log_Foo("Threads are in flight");
	DWORD w = WaitForMultipleObjects(THREADCOUNT, aThread, TRUE, INFINITE);
	if (w == -1) {
		Log_Error("Error waiting! %08x", GetLastError());
	}
	Log_Foo("Threads are complete");
	for (int i = 0; i < THREADCOUNT; i++) {
		CloseHandle(aThread[i]);
	}
	double best_time = times[0];
	for (int i = 1; i < THREADCOUNT; ++i) {
		if (times[i] < best_time) {
			best_time = times[i];
		}
	}
	Log_Write("Perf", "Best time was %fms", best_time * 1000.0);
	Log_Error("Oops! It's an error");
}
