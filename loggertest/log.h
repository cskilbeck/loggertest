#pragma once

#if !defined(_WINDOWS_)
#include <Windows.h>
#endif

#if !defined(__cplusplus)

void Log_OutputW(WCHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...);
void Log_OutputA(CHAR const *channel, CHAR const *text, CHAR const *file, int line, ...);

#if defined(UNICODE)
#define Log_Output Log_OutputW
#else
#define Log_Output Log_OutputA
#endif

#else

void Log_Output(WCHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...);
void Log_Output(CHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...);
void Log_Output(WCHAR const *channel, CHAR const *text, WCHAR const *file, int line, ...);
void Log_Output(CHAR const *channel, CHAR const *text, WCHAR const *file, int line, ...);
void Log_Output(WCHAR const *channel, WCHAR const *text, CHAR const *file, int line, ...);
void Log_Output(CHAR const *channel, WCHAR const *text, CHAR const *file, int line, ...);
void Log_Output(WCHAR const *channel, CHAR const *text, CHAR const *file, int line, ...);
void Log_Output(CHAR const *channel, CHAR const *text, CHAR const *file, int line, ...);

#if defined(_STRING_)

void Log_Output(std::wstring const &channel, std::string const &	text, CHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, std::wstring const &	text, CHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, CHAR const *			text, CHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, WCHAR const *			text, CHAR const *file, int line, ...);

void Log_Output(std::wstring const &channel, std::string const &	text, WCHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, std::wstring const &	text, WCHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, CHAR const *			text, WCHAR const *file, int line, ...);
void Log_Output(std::wstring const &channel, WCHAR const *			text, WCHAR const *file, int line, ...);

void Log_Output(std::string const &	channel, std::string const &	text, CHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, std::wstring const &	text, CHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, CHAR const *			text, CHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, WCHAR const *			text, CHAR const *file, int line, ...);

void Log_Output(std::string const &	channel, std::string const &	text, WCHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, std::wstring const &	text, WCHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, CHAR const *			text, WCHAR const *file, int line, ...);
void Log_Output(std::string const &	channel, WCHAR const *			text, WCHAR const *file, int line, ...);

void Log_Output(WCHAR const *		channel, std::string const &	text, CHAR const *file, int line, ...);
void Log_Output(WCHAR const *		channel, std::wstring const &	text, CHAR const *file, int line, ...);

void Log_Output(WCHAR const *		channel, std::string const &	text, WCHAR const *file, int line, ...);
void Log_Output(WCHAR const *		channel, std::wstring const &	text, WCHAR const *file, int line, ...);

void Log_Output(CHAR const *		channel, std::string const &	text, CHAR const *file, int line, ...);
void Log_Output(CHAR const *		channel, std::wstring const &	text, CHAR const *file, int line, ...);

void Log_Output(CHAR const *		channel, std::string const &	text, WCHAR const *file, int line, ...);
void Log_Output(CHAR const *		channel, std::wstring const &	text, WCHAR const *file, int line, ...);

#endif

namespace Log {
	std::string $(CHAR const *fmt, ...);
	std::wstring $(WCHAR const *fmt, ...);
}

// Channel: char const *, wchar const *, std::string const &, std::wstring const &
// Text: char const *, wchar const *, std::string const &, std::wstring const &
// File: char const *, wchar const *, std::string const &, std::wstring const &

#endif

#define Log_Write(str, txt, ...) Log_Output(str, txt, TEXT(__FILE__), __LINE__, __VA_ARGS__)

#define Log_Debug(txt, ...) Log_Write(TEXT("Debug"), txt, __VA_ARGS__)
#define Log_Info(txt, ...) Log_Write(TEXT("Info"), txt, __VA_ARGS__)
#define Log_Warning(txt, ...) Log_Write(TEXT("Warning"), txt, __VA_ARGS__)
#define Log_Error(txt, ...) Log_Write(TEXT("Error"), txt, __VA_ARGS__)

// to make your own
// #define Log_Foo(txt, ...) Log_Write(TEXT("Foo"), txt, __VA_ARGS__)

// or just use Log_Write directly
// Log_Write(TEXT("Foo"), TEXT("Message %d"), 1);
