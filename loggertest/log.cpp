#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include <memory>
#include <vector>
#include <string>

// message is a LogEntry

#pragma pack(push, 4)
struct LogEntry {
	FILETIME timestamp;
	UINT32 line_number;
	UINT32 channel_len;
	UINT32 text_len;
	UINT32 filename_len;
};
#pragma pack(pop)

namespace Log {

	std::string $(CHAR const *fmt, ...) {
		va_list v;
		va_start(v, fmt);
		size_t l = (size_t)(_vscprintf(fmt, v) + 1);
		std::string n(l, 0);
		if (l > 0) {
			_vsnprintf_s(&n[0], l, l - 1, fmt, v);
		}
		return n;
	}

	std::wstring $(WCHAR const *fmt, ...) {
		va_list v;
		va_start(v, fmt);
		size_t l = (size_t)(_vscwprintf(fmt, v) + 1);
		std::wstring n(l, 0);
		if (l > 0) {
			_vsnwprintf_s(&n[0], l, l - 1, fmt, v);
		}
		return n;
	}
}

// followed by the 3 UTF8 strings (channel, text, filename) without null termination

// total message size
static int const buf_size = 65536;

// message buffer
static std::vector<byte> send_buffer(buf_size);

// the pipe gets opened once and re-used
static HANDLE pipe_handle = NULL;

// did we try and open the pipe already?
static bool pipe_opened = false;

// base address of message buffer
static byte *buffer_base = (byte *)&send_buffer.at(0);

// it starts with a LogEntry
static LogEntry *log_entry = (LogEntry *)buffer_base;

// buffer to sprintf Text into
static WCHAR text_buffer[16384];

// max size of final Text
static const size_t text_buffer_size = 16384;

// the end of the buffer
static LPSTR buf_end = (LPSTR)buffer_base + buf_size;

struct Lock {
public:
	Lock() {
		mutex = CreateMutex(NULL, FALSE, NULL);
	}
	void Acquire() {
		WaitForSingleObject(mutex, INFINITE);
	}
	void Release() {
		ReleaseMutex(mutex);
	}
	HANDLE mutex;
};

struct Locker {
public:
	Locker(Lock &locker) : lock(locker) {
		lock.Acquire();
	}
	~Locker() {
		lock.Release();
	}
	Lock &lock;
};

static Lock pipe_lock;
static Lock file_lock;

// close the pipe when atexit()
static void ClosePipe() {
	if (pipe_handle != INVALID_HANDLE_VALUE && pipe_handle != NULL) {
		CloseHandle(pipe_handle);
	}
}

// Write some UTF8 to a file

static void WriteToFile(CHAR const *text) {
	Lock(file_lock);
	static DWORD mode = CREATE_ALWAYS;
	HANDLE file = CreateFile(TEXT("D:\\LogWindow.txt"), GENERIC_WRITE, FILE_SHARE_READ, NULL, mode, FILE_ATTRIBUTE_NORMAL, NULL);
	if (file != INVALID_HANDLE_VALUE) {
		mode = OPEN_EXISTING;
		DWORD wrote;
		WriteFile(file, text, strnlen_s(text, 65536), &wrote, NULL);
		CloseHandle(file);
	}
}

static void WriteLogEntryToFile(LogEntry &log_entry) {
	// TODO (chs): write the log entry as a csv compatible string
}

// try to open the pipe
// TODO (chs): what to do if pipe can't be opened, or debugger not attached?
static bool OpenPipe() {
	if (!pipe_opened) {
		pipe_opened = true;
		atexit(ClosePipe);
		DWORD process_id = GetCurrentProcessId();
		TCHAR pipe_name[64];
		_stprintf_s(pipe_name, TEXT("\\\\.\\pipe\\%08x_8834E49A-9D77-4166-AD66-EF29CBDA2B13"), process_id);
		pipe_handle = CreateFile(pipe_name, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (pipe_handle == INVALID_HANDLE_VALUE) {
			DWORD error = GetLastError();
			OutputDebugStringA(Log::$("Can't open pipe (%08x) - logging will go to OutputDebugString\n", error).c_str());
			printf("Can't open pipe (%08x) - logging will go to OutputDebugString\n", error);
			pipe_handle = NULL;
		}
	}
	return pipe_handle != NULL;
}

// add some ascii to the message buffer
int addToBuffer(LPSTR &buffer, CHAR const *text, int max) {
	int len = strnlen_s(text, max);
	memcpy(buffer, text, len);
	buffer += len;
	return len;
}

// add some UTF16 to the buffer
int addToBuffer(LPSTR &buffer, WCHAR const *text, int max) {
	int len = WideCharToMultiByte(CP_UTF8, 0, text, -1, buffer, max, NULL, NULL) - 1;
	buffer += len;
	return len;
}

// assemble a LogMessage
template <typename chan_t, typename text_t, typename file_t> void Writer(chan_t const *channel, text_t const *text, file_t const *file, int line, FILETIME const &timestamp) {
	LPSTR buffer_cur = (LPSTR)buffer_base;
	buffer_cur += sizeof(LogEntry);
	int channel_len = addToBuffer(buffer_cur, channel, buf_end - buffer_cur); // TODO (chs): check for buffer size limit
	int text_len = addToBuffer(buffer_cur, text, buf_end - buffer_cur);
	int filename_len = addToBuffer(buffer_cur, file, buf_end - buffer_cur);
	log_entry->timestamp = timestamp;
	log_entry->line_number = line;
	log_entry->channel_len = channel_len;
	log_entry->text_len = text_len;
	log_entry->filename_len = filename_len;
	DWORD buf_len = (byte *)buffer_cur - (byte *)buffer_base;



	DWORD wrote;
	WriteFile(pipe_handle, buffer_base, buf_len, &wrote, NULL);
}

// wide char format string
template <typename chan_t, typename file_t> void Log_OutputInternal(chan_t const *channel, WCHAR const *text, file_t const *file, int line, va_list v) {
	FILETIME timestamp;
	GetSystemTimeAsFileTime(&timestamp);
	Locker lock(pipe_lock);
	if (OpenPipe()) {
		vswprintf_s(text_buffer, text, v);
		Writer(channel, text_buffer, file, line, timestamp);
	}
}

// ascii format string
template <typename chan_t, typename file_t> void Log_OutputInternal(chan_t const *channel, CHAR const *text, file_t const *file, int line, va_list v) {
	FILETIME timestamp;
	GetSystemTimeAsFileTime(&timestamp);
	Locker lock(pipe_lock);
	if (OpenPipe()) {
		vsprintf_s((char * const)text_buffer, text_buffer_size, text, v);	// wrong size by half, but consistent 16k char limit
		Writer(channel, (LPSTR)text_buffer, file, line, timestamp);
	}
}

// output functions (channel, text can each be ascii or wide, string or [w]char const *, file can be ascii or wide char const *)

void Log_Output(WCHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(CHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(WCHAR const *channel, CHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(CHAR const *channel, CHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(WCHAR const *channel, WCHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(CHAR const *channel, WCHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(WCHAR const *channel, CHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

void Log_Output(CHAR const *channel, CHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}
void Log_Output(std::wstring const &channel, std::string const & text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::wstring const &channel, std::wstring const &text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::wstring const &channel, CHAR const * text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::wstring const &channel, WCHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::wstring const &channel, std::string const & text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::wstring const &channel, std::wstring const &text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::wstring const &channel, CHAR const * text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::wstring const &channel, WCHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::string const & channel, std::string const & text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::string const & channel, std::wstring const &text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::string const & channel, CHAR const * text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::string const & channel, WCHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::string const & channel, std::string const & text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::string const & channel, std::wstring const &text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text.c_str(), file, line, v);
}

void Log_Output(std::string const & channel, CHAR const * text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(std::string const & channel, WCHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel.c_str(), text, file, line, v);
}

void Log_Output(WCHAR const * channel, std::string const & text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(WCHAR const * channel, std::wstring const &text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(WCHAR const * channel, std::string const & text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(WCHAR const * channel, std::wstring const &text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(CHAR const *  channel, std::string const & text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(CHAR const *  channel, std::wstring const &text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(CHAR const *  channel, std::string const & text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

void Log_Output(CHAR const *  channel, std::wstring const &text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text.c_str(), file, line, v);
}

extern "C" void Log_OutputA(CHAR const *channel, CHAR const *text, CHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

extern "C" void Log_OutputW(WCHAR const *channel, WCHAR const *text, WCHAR const *file, int line, ...) {
	va_list v;
	va_start(v, line);
	Log_OutputInternal(channel, text, file, line, v);
}

